"""
'    ' 4 space
' ': space
'\n\n': 2 new line
'\n': new line

NUL	Null
SOH	Start of Header
STX	Start of Text
ETX	End of Text
EOT	End of Transmission
ENQ	Enquiry
ACK	Acknowledge
BEL	Bell
BS	Backspace
HT	Horizontal Tab
LF	Line Feed
VT	Vertical Tab
FF	Form Feed
CR	Carriage Return
SO	Shift Out
SI	Shift In
DLE	Data Link Escape
DC1	Device Control 1
DC2	Device Control 2
DC3	Device Control 3
DC4	Device Control 4
NAK	Negative Acknowledge
SYN	Synchronize
ETB	End of Transmission Block
CAN	Cancel
EM	End of Medium
SUB	Substitute
ESC	Escape
FS	File Separator
GS	Group Separator
RS	Record Separator
US	Unit Separator
space	Space
!	Exclamation mark
"	Double quote
#	Sharp
$	Dollar
%	Percent
&	Ampersand
'	Single quote
(	Left round bracket
)	Right round bracket
*	Asterisk
+	Plus
,	Comma
-	Minus
.	Dot
/	Slash
0	Zero
1	One
2	Two
3	Three
4	Four
5	Five
6	Six
7	Seven
8	Eight
9	Nine
:	Colon
;	Semicolon
<	Less than
=	Equal
>	Greater than
?	Question mark
@	At symbol
A	Capital A
B	Capital B
C	Capital C
D	Capital D
E	Capital E
F	Capital F
G	Capital G
H	Capital H
I	Capital I
J	Capital J
K	Capital K
L	Capital L
M	Capital M
N	Capital N
O	Capital O
P	Capital P
Q	Capital Q
R	Capital R
S	Capital S
T	Capital T
U	Capital U
V	Capital V
W	Capital W
X	Capital X
Y	Capital Y
Z	Capital Z
[	Left square bracket
\	Backslash
]	Right square bracket
^	Caret symbol
_	Underline
`	Grave accent symbol
a	a
b	b
c	c
d	d
e	e
f	f
g	g
h	h
i	i
j	j
k	k
l	l
m	m
n	n
o	o
p	p
q	q
r	r
s	s
t	t
u	u
v	v
w	w
x	x
y	y
z	z
{	Left curly bracket
|	Vertical bar
}	Right curly bracket
~	Tilde symbol
DEL	Delete
"""
